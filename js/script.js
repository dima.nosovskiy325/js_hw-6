/* 
 Теоретичні питання
 1. Опишіть своїми словами, що таке метод об'єкту
 2. Який тип даних може мати значення властивості об'єкта?
 3. Об'єкт це посилальний тип даних. Що означає це поняття?

 Практичні завдання
 1. Створіть об'єкт product з властивостями name, price та discount. Додайте метод для виведення повної ціни товару з урахуванням знижки. Викличте цей метод та результат виведіть в консоль.
 
 2. Напишіть функцію greeting, яка приймає об'єкт з властивостями name та age, і повертає рядок з привітанням і віком, 
 наприклад "Привіт, мені 30 років". 
 Попросіть користувача ввести своє ім'я та вік 
 за допомогою prompt, і викличте функцію gteeting з введеними даними(передавши їх як аргументи). Результат виклику функції виведіть з допомогою alert.

3.Опціональне. Завдання:
Реалізувати повне клонування об'єкта. 

Технічні вимоги:
- Написати функцію для рекурсивного повного клонування об'єкта (без єдиної передачі за посиланням, внутрішня вкладеність властивостей об'єкта може бути досить великою).
- Функція має успішно копіювати властивості у вигляді об'єктів та масивів на будь-якому рівні вкладеності.
- У коді не можна використовувати вбудовані механізми клонування, такі як функція Object.assign() або spread.
 */

/* 

Теория

1) Метод объекта, это по сути функция которая выполняет что-либо внутри обьекта
2) Значение свойства в объекте может иметь любой тип данных, то есть примитивные типы, так же объекты, массивы или даже функции 
3) Это значит что при создании объекта в переменной, мы присваиваем ей не сам объект, а ссылку на него, таким образом переменная указывает на место в памяти, где хранится этот объект.


*/

// практика

// 1

const product = {
  name: "",
  price: 1000,
  discount: 15,

  getPriceWithDiscount: function () {
    let discountedPrice = this.price * (1 - this.discount / 100);
    console.log(
      `Цена товара ${this.name} с учетом скидки: ${discountedPrice.toFixed(2)}$`
    );
    return discountedPrice;
  },
};

product.getPriceWithDiscount();

// 2

const user = {
  name: "",
  age: "",
};

function greeting(obj) {
  return `Привіт, мене звати ${obj.name} і мені ${obj.age} років`;
}

user.name = prompt("введите своё имя", "");
user.age = prompt("введите свой возраст", "");

alert(greeting(user));
